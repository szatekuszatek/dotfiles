require('bartosz')
local utils = require('bartosz.utils')
require('ayu')
local builtin = require('telescope.builtin')

vim.g.rnvimr_enable_picker = 1

vim.g.mapleader = " "

utils.nmap('<leader>pv', ':lua MiniFiles.open()<CR>')
utils.nmap('<C-j>', ':cnext<CR>')
utils.nmap('<C-k>', ':cprev<CR>')
utils.vmap('<leader>p', '"_dP')
utils.vmap('<leader>y', '"+y')
utils.nmap('<C-t>', ':silent !tmux neww tmux_sessionizer<CR>')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fs', builtin.git_files, {})
vim.keymap.set('n', '<leader>fl', builtin.live_grep, {})
