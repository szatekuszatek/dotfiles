require('ayu').setup({
    overrides = function()
        return { LineNr = { fg = '#A0A1A4'}}
    end
})
require('ayu').colorscheme({})
require('lualine').setup({
  options = {
    theme = 'ayu',
  },
})
require('Comment').setup()
require('autoclose').setup()
require('mini.files').setup()
