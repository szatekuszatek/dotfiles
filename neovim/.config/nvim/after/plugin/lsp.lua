local lsp_zero = require('lsp-zero')

vim.g.rustaceanvim = {
  server = {
    capabilities = lsp_zero.get_capabilities()
  },
}

require('mason').setup({})
require('mason-lspconfig').setup({
  ensure_installed = {
    'ts_ls',
    'eslint',
    'lua_ls',
    'bashls'
  },
  handlers = {
    lsp_zero.default_setup,
    rust_analyzer = lsp_zero.noop,
  }
})


local cmp = require('cmp')
local cmp_format = lsp_zero.cmp_format()

cmp.setup({
    sources = {
        { name = 'nvim_lsp' }
    },
  formatting = cmp_format,
  mapping = cmp.mapping.preset.insert({
    -- scroll up and down the documentation window
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
    ['<CR>'] = cmp.mapping.confirm({ select = true })
  }),
})

local lsp = lsp_zero.preset({})
-- Format on save
lsp.format_on_save({
    format_opts = {
        async = false,
        timeout_ms = 10000,
    },
    servers = {
        ["rust-analyzer"] = { "rust" },
    }
})

-- local null_ls = require('null-ls')
-- local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
--
-- null_ls.setup({
--   sources = {
--     null_ls.builtins.formatting.prettier,
--     null_ls.builtins.formatting.clang_format,
--     null_ls.builtins.code_actions.gitsigns,
--     require("none-ls.formatting.rustfmt"),
--   },
--   on_attach = function(client, bufnr)
--     if client.supports_method("textDocument/formatting") then
--         vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
--         vim.api.nvim_create_autocmd("BufWritePre", {
--             group = augroup,
--             buffer = bufnr,
--             callback = function()
--                 vim.lsp.buf.format({ bufnr = bufnr })
--             end,
--         })
--     end
-- end,
-- })
