local bufnr = vim.api.nvim_get_current_buf()
local opts = { silent = true, buffer = bufnr }
vim.keymap.set("n", "<leader>a",
  function()
      vim.cmd.RustLsp('codeAction')
  end,
  opts
)
vim.keymap.set("n", "K",
  function()
    vim.cmd.RustLsp { 'hover', 'actions' }
  end,
  opts
)
vim.keymap.set('n', 'gD',
function()
    vim.lsp.buf.declaration()
end,
opts)
vim.keymap.set('n', 'gd',
function()
    vim.lsp.buf.definition()
end,
opts)
vim.keymap.set('n', 'gi',
function()
    vim.lsp.buf.implementation()
end,
opts)
vim.keymap.set('n', 'gl',
function()
    vim.cmd.RustLsp({ 'renderDiagnostic', 'current' })
end,
opts)

for _, method in ipairs({ 'textDocument/diagnostic', 'workspace/diagnostic' }) do
    local default_diagnostic_handler = vim.lsp.handlers[method]
    vim.lsp.handlers[method] = function(err, result, context, config)
        if err ~= nil and err.code == -32802 then
            return
        end
        return default_diagnostic_handler(err, result, context, config)
    end
end
