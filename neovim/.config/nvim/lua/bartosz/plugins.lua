vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'

  use 'Shatur/neovim-ayu'
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  }
  use {
   'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  use {
    "NeogitOrg/neogit",
    requires = {
        "nvim-lua/plenary.nvim",
        "sindrets/diffview.nvim"
    }
  }
  use 'echasnovski/mini.nvim'
  use {'williamboman/mason.nvim', run = function() pcall(vim.cmd, 'MasonUpdate') end}
  use 'williamboman/mason-lspconfig.nvim'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use {
      'VonHeikemen/lsp-zero.nvim',
      requires = { {'neovim/nvim-lspconfig'} }
  }
  use 'mrcjkb/rustaceanvim'
  use {
    'lewis6991/gitsigns.nvim',
    config = function()
      require('gitsigns').setup()
    end
  }
  use 'ziglang/zig.vim'
  -- formatting
  use 'm4xshen/autoclose.nvim'
  use 'numToStr/Comment.nvim'
  use {'nvimtools/none-ls.nvim', requires = {"nvimtools/none-ls-extras.nvim"}}
  use 'yamatsum/nvim-cursorline'

end)
