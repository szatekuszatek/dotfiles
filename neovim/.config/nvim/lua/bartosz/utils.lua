local utils = {}

function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

function utils.nmap(shortcut, command)
  map('n', shortcut, command)
end

function utils.imap(shortcut, command)
  map('i', shortcut, command)
end

function utils.vmap(shortcut, command)
  map('v', shortcut, command)
end

return utils
